<?php 
require_once (realpath('header.php'));

/*
Установка пакетов
https://modx.pro/solutions/6723-the-script-auto-setup-component-in-modx/
*/
$providers = array(
	array(
		'name'			=> 'modx.com',
		'service_url'	=> 'http://rest.modx.com/extras/',
		'username'		=> '',
		'api_key'		=> '',
		'packages'		=> array(
			'translit',
		),
	),
	array(
		'name'			=> 'ModStore',
		'service_url'	=> 'https://modstore.pro/extras/',
		'username'		=> '',
		'api_key'		=> '',
		'packages'		=> array(
			'pdotools',
			'tickets',
			'hybridauth',
			'ace',
			//'bannery',
			'minifyx',
			'ajaxsnippet',
		),
	),
);



$countInstalled = 0;

$console = !empty($argv) ? true : false; 

if( $console )
{
	if( !empty($argv[1]) && file_exists( $argv[1] .'index.php' ) )
	{
		$current_dir = $argv[1];
	}
}



$modx->addPackage('modx.transport', MODX_CORE_PATH .'model/');

$modx->getVersionData();
$productVersion = $modx->version['code_name'] .'-'. $modx->version['full_version'];

foreach( $providers as $prov )
{
	if( !$provider = $modx->getObject('transport.modTransportProvider', array('service_url' => $prov['service_url']) ) )
	{
		$provider = $modx->newObject('transport.modTransportProvider');
		
		$provider->fromArray($prov);
		$provider->save();
	}
	
	$provider->getClient();
	
	foreach( $prov['packages'] as $packageName )
	{
		$response = $provider->request('package', 'GET',
			array(
				'query' => $packageName
			));
		
		if( !empty($response) )
		{
			$foundPackages = simplexml_load_string($response->response);
			
			if( $foundPackages['total'] > 0 )
			{
				foreach( $foundPackages as $foundPackage )
				{
					
					if( strtolower((string)$foundPackage->name) == strtolower($packageName) )
					{
						if( !$modx->getCount('transport.modTransportPackage', array('signature' => (string)$foundPackage->signature)) )
						{
							$sig = explode('-', $foundPackage->signature);
							$versionSignature = explode('.', $sig[1]);
							
							file_put_contents( MODX_CORE_PATH .'packages/'. $foundPackage->signature .'.transport.zip', file_get_contents($foundPackage->location) );
							
							$package = $modx->newObject('transport.modTransportPackage');
							
							$package->set('signature', $foundPackage->signature);
							
							$package->fromArray(
								array(
									'created'		=> date('Y-m-d h:i:s'),
									'updated'		=> null,
									'state'			=> 1,
									'workspace'		=> 1,
									'provider'		=> $provider->id,
									'source'		=> $foundPackage->signature .'.transport.zip',
									'package_name'	=> (string)$foundPackage->name,
									'version_major'	=> $versionSignature[0],
									'version_minor'	=> !empty($versionSignature[1]) ? $versionSignature[1] : 0,
									'version_patch'	=> !empty($versionSignature[2]) ? $versionSignature[2] : 0,
								));
							
							if( !empty($sig[2]) )
							{
								$r = preg_split('/([0-9]+)/', $sig[2], -1, PREG_SPLIT_DELIM_CAPTURE);
								
								if( is_array($r) && !empty($r) )
								{
									$package->set('release', $r[0]);
									$package->set('release_index', (isset($r[1]) ? $r[1] : '0'));
								}
								else {
									$package->set('release', $sig[2]);
								}
								
								if( $success = $package->save() )
								{
									$package->install();
									_print('Installed: "'. (string)$foundPackage->name .'"');
									$countInstalled++;
								}
								else {
									_print('Could not save: "'. (string)$foundPackage->name .'"');
								}
							}
						}
						else {
							_print('Already exists: "'. (string)$foundPackage->name .'"');
						}
					}
				}
			}
			else {
				_print('Not found: "'. $packageName .'"');
			}
		}
	}
}

if( $countInstalled > 0 )
{
	_print('Done!', 0);
}



function _print( $str='', $br=true )
{
	global $console;
	
	if( empty($str) ) {return;}
	
	if( $console )
	{
		fwrite(STDOUT, $str . ($br ? "\n" : ""));
	}
	else {
		print $str . ($br ? "\n" : "");
	}
}

exit;

