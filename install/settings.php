<?php
require_once (realpath('header.php'));
/*
Редактирование настроек
https://modx.pro/solutions/6723-the-script-auto-setup-component-in-modx/
*/
// Settings
$settings = array(
    //pdoTools
    'pdotools_fenom_modx' => '1',
    'pdotools_fenom_parser' => '1',
    //Визуальный редактор
    'which_element_editor' => 'Ace',
    'which_editor' => 'Не указано',
    'use_editor' => '0',
    //Кэширование
    'cache_db' => '1',
    //URL
    'friendly_urls' => '1',
    'friendly_urls_strict' => '1',
    'global_duplicate_uri_check' => '1',
    'use_alias_path' => '1',
    'friendly_alias_translit' => 'russian',
    //Панель управления
    'topmenu_show_descriptions' => '0',
    //Авторизация и безопасность
    'allow_multiple_emails' => 0,
    //Сайт
    'site_name' => 'Startboxcms',
    'site_meta_description' => '',
    'site_template' => 'default',
    'site_layout' => 'default',
    'site_theme' => 'almost-flat',
    'site_description' => 'MODX Revolution Templates',
    'default_template' => '2',
    'hidemenu_default' => '1',
    //Ace
    'ace.theme' => 'solarized_dark',
    //HybridAuth
    'ha.frontend_css' => '',
    //Tickets
    // 'tickets.frontend_css' => '',
    'tickets.frontend_js' => '',
    'tickets.count_guests' => '1',
    'tickets.default_template' => '3',
    'tickets.section_content_default' => "{\$_modx->getChunk('@FILE default/default/actions/tickets.list.tpl')}",

);
foreach ($settings as $k => $v) {
    $opt = $modx->getObject('modSystemSetting', array('key' => $k));
    if (!empty($opt)){
        $opt->set('value', $v);
        $opt->save();
        echo 'edited '.$k.' = '.$v."\n";
    } else {
        $newOpt = $modx->newObject('modSystemSetting');
        $newOpt->set('key', $k);
        $newOpt->set('value', $v);
        $newOpt->set('area', 'site');
        $newOpt->save();
        echo 'added '.$k.' = '.$v."\n";
    }
}

$modx->cacheManager->refresh();
exit();
