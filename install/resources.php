<?php  
require_once (realpath('header.php'));
$resources = array (
    array (
		'id' => '1',
		'pagetitle' => 'Главная',
		'menutitle' => '<i class="uk-icon-home"></i> Главная',
		'hidemenu' => 0,
		'alias' => 'index',
		'context_key' => 'web'
      )
    );
foreach ($resources as $resource) {
$response = $modx->runProcessor('resource/update', $resource);
$resourceArray = $response->getObject();
echo 'The resource ID '.$resourceArray['id'].' update '."\n";
}

$resources = array (
    array (
		'pagetitle' => 'Профиль',
		'menutitle' => '<i class="uk-icon-user"></i> Профиль',
		'hidemenu' => 1,
		'alias' => 'profile',
		'content' => "{\$_modx->getChunk('@FILE default/default/actions/profile.tpl')}",
		'template' => 2,
		'published' => 1,
		'context_key' => 'web'
      ),
    array (
		'pagetitle' => 'Добавить запись',
		'menutitle' => '<i class="uk-icon-plus"></i> Добавить запись',
		'hidemenu' => 0,
		'alias' => 'add-post',
		'content' => "{\$_modx->getChunk('@FILE default/default/actions/add.post.tpl')}",
		'parent' => '2',
		'template' => 2,
		'published' => 1,
		'context_key' => 'web'
      ),
    array (
		'pagetitle' => 'Мои записи',
		'menutitle' => '<i class="uk-icon-edit"></i> Мои записи',
		'hidemenu' => 0,
		'alias' => 'my-posts',
		'content' => "{\$_modx->getChunk('@FILE default/default/actions/my.posts.tpl')}",
		'parent' => '2',
		'template' => 2,
		'published' => 1,
		'context_key' => 'web'
      ),
    array (
		'pagetitle' => 'Черновики',
		'menutitle' => '<i class="uk-icon-file-o"></i> Черновики',
		'hidemenu' => 0,
		'alias' => 'my-drafts',
		'content' => "{\$_modx->getChunk('@FILE default/default/actions/my.drafts.tpl')}",
		'parent' => '2',
		'template' => 2,
		'published' => 1,
		'context_key' => 'web'
      ),
    array (
		'pagetitle' => 'Избранное',
		'menutitle' => '<i class="uk-icon-star"></i> Избранное',
		'hidemenu' => 0,
		'alias' => 'my-favorites',
		'content' => "{\$_modx->getChunk('@FILE default/default/actions/my.favorites.tpl')}",
		'parent' => '2',
		'template' => 2,
		'published' => 1,
		'context_key' => 'web'
      ),
     array (
		'pagetitle' => 'Разделы',
		'menutitle' => '<i class="uk-icon-sitemap"></i> Разделы',
		'hidemenu' => 0,
		'alias' => 'blogs',
		'content' => "{\$_modx->getChunk('@FILE default/default/actions/sections.tpl')}",
		'template' => 2,
		'published' => 1,
		'context_key' => 'web'
       ),
     array (
		'pagetitle' => 'Блог',
		'menutitle' => '<i class="uk-icon-comments-o"></i> Блог',
		'alias' => 'blog',
		'content' => "{\$_modx->getChunk('@FILE default/default/actions/tickets.list.tpl')}",
		'parent' => '7',
		'template' => 2,
		'published' => 1,
		'class_key' => 'TicketsSection',
		'context_key' => 'web'
       ),
    );
foreach ($resources as $resource) {
$response = $modx->runProcessor('resource/create', $resource);
$resourceArray = $response->getObject();
echo 'The resource ID '.$resourceArray['id'].' add '."\n";
}

