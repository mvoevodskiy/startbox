<?php  
require_once (realpath('header.php'));
$templates = array (
    array (
		'id' => '1',
		'templatename' => 'Главная страница',
		'description' => '',
		'content' => '',
		'static' => 1,
		'source' => 1,
		'static_file' => '[[++assets_url]]/elements/templates/[[++site_template]]/layout/[[++site_layout]]/index.tpl',
      )
    );
foreach ($templates as $template) {
$response = $modx->runProcessor('element/template/update', $template);
$templateArray = $response->getObject();
echo 'The template ID '.$templateArray['id'].' update '."\n";
}


$templates = array (
    array (
		'templatename' => 'Внутренняя страница',
		'description' => '',
		'content' => '',
		'static' => 1,
		'source' => 1,
		'static_file' => '[[++assets_url]]/elements/templates/[[++site_template]]/layout/[[++site_layout]]/inner.page.tpl',
      ),
    array (
		'templatename' => 'Страница Тикета',
		'description' => '',
		'content' => '',
		'static' => 1,
		'source' => 1,
		'static_file' => '[[++assets_url]]/elements/templates/[[++site_template]]/layout/[[++site_layout]]/ticket.page.tpl',
      )
    );
foreach ($templates as $template) {
$response = $modx->runProcessor('element/template/create', $template);
$templateArray = $response->getObject();
echo 'The template ID '.$templateArray['id'].' add '."\n";
}

