<?php 
header("Content-type: text/plain; charset=utf-8");
require_once(realpath('../config.core.php'));
include_once MODX_CORE_PATH . 'model/modx/modx.class.php';
$modx= new modX();
$modx->initialize('mgr');
$modx->getService('error','error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');

if(!$modx->user->hasSessionContext('mgr')){
die("Администратор не авторизован!");
}
?>
